package cz.sep.tadeas.primitives;

import cz.sep.tadeas.primitives.User;
import org.springframework.format.datetime.joda.DateTimeFormatterFactoryBean;

import java.util.Date;

public class Task {

    private float id;
    private String name;
    private User issuer;
//    private Date issueDate;
    private String issueDate;
    private boolean active;
    private String definition;

    public Task() {
    }

    public Task(String name) {
        this.name = name;
    }

    public Task(float id,
                String name,
                User issuer,
                String issueDate,
                boolean active,
                String definition) {
        this.id = id;
        this.name = name;
        this.issuer = issuer;
        this.issueDate = issueDate;
        this.active = active;
        this.definition = definition;
    }

    public float getId() {
        return id;
    }

    public void setId(float id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getIssuer() {
        return issuer;
    }

    public void setIssuer(User issuer) {
        this.issuer = issuer;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getDefinition() {
        return definition;
    }

    public void setDefinition(String definition) {
        this.definition = definition;
    }

    @Override
    public String toString() {
        return name;
    }

    public String toStringAll() {
        return "Task{\n" +
                "id=" + id +
                ", \nname='" + name + '\'' +
                ", \nissuer=" + issuer +
                ", \nissueDate='" + issueDate + '\'' +
                ", \nactive=" + active +
                ", \ndefinition='" + definition + '\'';
    }
}
