package cz.sep.tadeas.primitives;

import java.util.ArrayList;

public class User {

    private float id;
    private String userName;
    private String firstName, lastName;
    private String email;
    private String password;
    private UserRole role;
    private ArrayList<UserGroup> groups;

    public User() {
    }

    public User(float id,
                String userName,
                String email,
                String password,
                UserRole role) {
        this.id = id;
        this.userName = userName;
        this.email = email;
        this.password = password;
        this.role = role;
    }

    public User(float id,
                String userName,
                String firstName,
                String email,
                String password,
                UserRole role) {
        this.id = id;
        this.userName = userName;
        this.firstName = firstName;
        this.email = email;
        this.password = password;
        this.role = role;
    }

    public User(float id,
                String userName,
                String firstName,
                String lastName,
                String email,
                String password,
                UserRole role) {
        this.id = id;
        this.userName = userName;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.role = role;
    }

    public User(float id,
                String userName,
                String firstName,
                String lastName,
                String email,
                String password,
                UserRole role,
                ArrayList<UserGroup> groups) {
        this.id = id;
        this.userName = userName;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.role = role;
        this.groups = groups;
    }

    public float getId() {
        return id;
    }

    public void setId(float id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public ArrayList<UserGroup> getGroups() {
        return groups;
    }

    public void setGroups(ArrayList<UserGroup> groups) {
        this.groups = groups;
    }

    @Override
    public String toString() {
        return  userName + ", email='" + email + '\'';
    }

    public String toStringAll() {
        return "User{" +
                "id=" + id +
                ", userName='" + userName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", role=" + role +
                ", groups=" + groups +
                '}';
    }
}
