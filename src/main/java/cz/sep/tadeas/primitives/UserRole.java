package cz.sep.tadeas.primitives;

public class UserRole {

    private float id;
    private String name;

    public UserRole() {
    }

    public UserRole(float id, String name) {
        this.id = id;
        this.name = name;
    }

    public float getId() {
        return id;
    }

    public void setId(float id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isStudent() {
        return id == 2;
    }

    public boolean isTeacher() {
        return id == 1;
    }

    @Override
    public String toString() {
        return "UserRole: " + name;
    }
}
