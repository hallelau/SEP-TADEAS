package cz.sep.tadeas.primitives;

import java.util.Arrays;

/**
 * Created by Laura on 24. 12. 2017.
 */
public class TaskDelivery {

    private float id;
    private TaskDeliveryWindow taskDeliveryWindow;
    private byte[] solution;
    private String deliveryDate;
    private User deliveryUser;
    private boolean valid;
    private String acceptanceDate;
    private User acceptanceUser;
    private String acceptanceMessage;
    private byte[] acceptanceBinary;
    private boolean Acceptation;

    public TaskDelivery() {
    }

    public float getId() {
        return id;
    }

    public void setId(float id) {
        this.id = id;
    }

    public TaskDeliveryWindow getTaskDeliveryWindow() {
        return taskDeliveryWindow;
    }

    public void setTaskDeliveryWindow(TaskDeliveryWindow taskDeliveryWindow) {
        this.taskDeliveryWindow = taskDeliveryWindow;
    }

    public byte[] getSolution() {
        return solution;
    }

    public void setSolution(byte[] solution) {
        this.solution = solution;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public User getDeliveryUser() {
        return deliveryUser;
    }

    public void setDeliveryUser(User deliveryUser) {
        this.deliveryUser = deliveryUser;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public String getAcceptanceDate() {
        return acceptanceDate;
    }

    public void setAcceptanceDate(String acceptanceDate) {
        this.acceptanceDate = acceptanceDate;
    }

    public User getAcceptanceUser() {
        return acceptanceUser;
    }

    public void setAcceptanceUser(User acceptanceUser) {
        this.acceptanceUser = acceptanceUser;
    }

    public String getAcceptanceMessage() {
        return acceptanceMessage;
    }

    public void setAcceptanceMessage(String acceptanceMessage) {
        this.acceptanceMessage = acceptanceMessage;
    }

    public byte[] getAcceptanceBinary() {
        return acceptanceBinary;
    }

    public void setAcceptanceBinary(byte[] acceptanceBinary) {
        this.acceptanceBinary = acceptanceBinary;
    }

    public boolean isAcceptation() {
        return Acceptation;
    }

    public void setAcceptation(boolean acceptation) {
        Acceptation = acceptation;
    }

    @Override
    public String toString() {
        return "TaskDelivery: \n" +
                "id=" + id +
                ", \ntaskDeliveryWindow=" + taskDeliveryWindow +
                ", \nsolution=" + Arrays.toString(solution) +
                ", \ndeliveryDate='" + deliveryDate + '\'' +
                ", \ndeliveryUser=" + deliveryUser +
                ", \nvalid=" + valid +
                ", \nacceptanceDate='" + acceptanceDate + '\'' +
                ", \nacceptanceUser=" + acceptanceUser +
                ", \nacceptanceMessage='" + acceptanceMessage + '\'' +
                ", \nacceptanceBinary=" + Arrays.toString(acceptanceBinary) +
                ", \nAcceptation=" + Acceptation;
    }
}
