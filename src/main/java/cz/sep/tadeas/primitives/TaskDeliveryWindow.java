package cz.sep.tadeas.primitives;

/**
 * Created by Laura on 24. 12. 2017.
 */
public class TaskDeliveryWindow {

    private float id;
    private Task task;
    private User issuer;
    private User solver;
    private String issueDate;
    private String startDate;
    private String deadlineDate;
    private boolean active;
    private String name;
    private String definition;
    private TaskDelivery predecessorDelivery;

    public TaskDeliveryWindow() {
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public TaskDelivery getPredecessorDelivery() {
        return predecessorDelivery;
    }

    public void setPredecessorDelivery(TaskDelivery predecessorDelivery) {
        this.predecessorDelivery = predecessorDelivery;
    }

    public float getId() {
        return id;
    }

    public void setId(float id) {
        this.id = id;
    }

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    public User getIssuer() {
        return issuer;
    }

    public void setIssuer(User issuer) {
        this.issuer = issuer;
    }

    public User getSolver() {
        return solver;
    }

    public void setSolver(User solver) {
        this.solver = solver;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getDeadlineDate() {
        return deadlineDate;
    }

    public void setDeadlineDate(String deadlineDate) {
        this.deadlineDate = deadlineDate;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDefinition() {
        return definition;
    }

    public void setDefinition(String definition) {
        this.definition = definition;
    }

    @Override
    public String toString() {
        return "TaskDeliveryWindow{" +
                "task=" + task +
                ", issuer=" + issuer +
                ", solver=" + solver +
                ", deadlineDate='" + deadlineDate + '\'' +
                '}';
    }
}
