package cz.sep.tadeas.ui;

import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.*;
import cz.sep.tadeas.*;

import com.vaadin.annotations.Theme;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;

import javax.servlet.annotation.WebServlet;

@SpringUI
@Theme("valo")
public class VaadinUI extends UI {

    @WebServlet(value = "/*", asyncSupported = true)
    @VaadinServletConfiguration(productionMode = false, ui = VaadinUI.class)
    public static class Servlet extends VaadinServlet {
    }

    public static Authentication AUTH;
    public static CurrentUser currentUser;

    @Override
    protected void init(VaadinRequest request) {
        AUTH = new Authentication();
        new Navigator(this, this);

        currentUser = new CurrentUser();

        getNavigator().addView(LoginPage.NAME, LoginPage.class);
        getNavigator().setErrorView(LoginPage.class);

        router("");

    }

    private void router(String route) {
        System.out.println("router: " + route);

        Notification.show(route);
        if (getSession().getAttribute("user") == null) {
            getNavigator().navigateTo(LoginPage.NAME);
        }
    }
}