package cz.sep.tadeas.ui;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FileResource;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.*;
import cz.sep.tadeas.LoginPage;
import cz.sep.tadeas.RestClient;
import cz.sep.tadeas.primitives.Task;
import cz.sep.tadeas.primitives.TaskDelivery;

import javax.swing.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static cz.sep.tadeas.ui.VaadinUI.currentUser;

public class StudentMainUI extends VerticalLayout implements View {

    public static final String NAME = "StudentMainUI";

    private Grid<Task> taskGrid;
    private List<Task> tasks;

    public StudentMainUI(){
        displayAllTasks();
        displayTasksSubmitions();
    }

    private void displayTasksSubmitions(){
        for (Task task : tasks){
            if (!task.isActive()) continue;

            Panel panel = new Panel("Submitting active task: " + task.getName());
            addComponent(panel);

            VerticalLayout mainLayout = new VerticalLayout();
            mainLayout.setSizeFull();
            mainLayout.setResponsive(true);

            panel.setContent(mainLayout);
            setComponentAlignment(panel, Alignment.MIDDLE_CENTER);

            TextArea taskInfoLabel = new TextArea("Info about task:");
            taskInfoLabel.setReadOnly(true);
            taskInfoLabel.setValue(task.toStringAll());
            taskInfoLabel.setSizeFull();
            mainLayout.addComponent(taskInfoLabel);

            class FileUploader implements Upload.Receiver, Upload.SucceededListener {
                public File file;
                public OutputStream receiveUpload(String filename,
                                                  String mimeType) {
                    File temp = null;
                    try {
                        temp = File.createTempFile("upload", null);
                    System.out.println(temp.getAbsolutePath());
                    this.file = temp;
                    return new FileOutputStream(temp);
                    } catch (IOException e) {
                        Notification.show("ERROR creating tmp file on server...", Notification.Type.ERROR_MESSAGE);
                        e.printStackTrace();
                    }

                    Notification.show("ERROR while uploading...", Notification.Type.ERROR_MESSAGE);
                    return null;
                }

                public void uploadSucceeded(Upload.SucceededEvent event) {
                    System.out.println("uploadSucceeded");
                    try {
                        createAndSendTaskDelivery(Files.readAllBytes(file.toPath()), task);
                        Notification.show("File successfully loaded", Notification.Type.HUMANIZED_MESSAGE);
                        mainLayout.addComponent(new Label("OK"));
                    } catch (IOException e) {
                        Notification.show("ERROR while uploading...", Notification.Type.ERROR_MESSAGE);
                        e.printStackTrace();
                    }
                }
            };
            FileUploader receiver = new FileUploader();

            Upload upload = new Upload("Upload", receiver);
            upload.addSucceededListener(receiver);
            mainLayout.addComponent(upload);
        }
    }

    private void createAndSendTaskDelivery(byte[] fileByteArr, Task task){
        TaskDelivery taskDelivery = new TaskDelivery();
        int newDeliveryId = 1;
        taskDelivery.setSolution(fileByteArr);
        taskDelivery.setId(newDeliveryId);
        taskDelivery.setDeliveryUser(currentUser);
        taskDelivery.setDeliveryDate("2013-07-27T17:13:59.985Z");
        RestClient restClient = new RestClient();

        ObjectMapper mapper = new ObjectMapper();
        try {
            restClient.put("/delivery/" + newDeliveryId, mapper.writeValueAsString(taskDelivery));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

    }

    private void displayAllTasks(){
        Panel panel = new Panel("All tasks");
        addComponent(panel);

        VerticalLayout mainLayout = new VerticalLayout();
        mainLayout.setSizeFull();
        mainLayout.setResponsive(true);

        panel.setContent(mainLayout);
        setComponentAlignment(panel, Alignment.MIDDLE_CENTER);

        mainLayout.addComponent(createGridOfTasks());
    }

    private void loadAllTasks() {
        RestClient restClient = new RestClient();
        String response = restClient.get("/task");

        ObjectMapper mapper = new ObjectMapper();

        try {
            tasks = mapper.readValue(response, new TypeReference<List<Task>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Component createGridOfTasks() {
        taskGrid = new Grid<>(Task.class);
        loadAllTasks();
        taskGrid.setItems(tasks);
        taskGrid.setResponsive(true);
        taskGrid.setSizeFull();
        return taskGrid;
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {
        Button logout = new Button("Logout");
        logout.addClickListener(new Button.ClickListener() {
            private static final long serialVersionUID = 1L;

            @Override
            public void buttonClick(Button.ClickEvent event) {
//                getUI().getNavigator().removeView(TeacherMainUI.NAME);
                VaadinSession.getCurrent().setAttribute("user", null);
                Page.getCurrent().setUriFragment("!" + LoginPage.NAME);
            }
        });

        addComponent(logout);
    }


}
