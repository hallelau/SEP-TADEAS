package cz.sep.tadeas.ui;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.*;
import cz.sep.tadeas.RestClient;
import cz.sep.tadeas.primitives.Task;

import java.io.IOException;
import java.util.List;

public class TasksUI extends VerticalLayout implements View {

    public static final String NAME = "TasksUI";

    private Grid<Task> taskGrid;
    private List<Task> tasks;

    public TasksUI() {
        tasks = null;

        Panel panel = new Panel("Tasks");
        addComponent(panel);

        VerticalLayout mainLayout = new VerticalLayout();
        mainLayout.setSizeFull();
        mainLayout.setResponsive(true);

        panel.setContent(mainLayout);
        setComponentAlignment(panel, Alignment.MIDDLE_CENTER);

        try {
            mainLayout.addComponent(createGridOfTasks());
        } catch (Exception e) {
            System.err.println("ERROR loading grid of tasks");
            e.printStackTrace();
        }
    }

    private void loadAllTasks() {
        RestClient restClient = new RestClient();
        String response = restClient.get("/task");

        ObjectMapper mapper = new ObjectMapper();

        try {
            tasks = mapper.readValue(response, new TypeReference<List<Task>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Component createGridOfTasks() {
        taskGrid = new Grid<>(Task.class);
        loadAllTasks();
        taskGrid.setItems(tasks);
        taskGrid.setResponsive(true);
        taskGrid.setSizeFull();
        return taskGrid;
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {

    }
}