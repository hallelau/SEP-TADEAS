package cz.sep.tadeas.ui;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.*;
import cz.sep.tadeas.LoginPage;
import cz.sep.tadeas.RestClient;
import cz.sep.tadeas.primitives.Task;
import cz.sep.tadeas.primitives.TaskDelivery;
import cz.sep.tadeas.primitives.TaskDeliveryWindow;

import java.io.IOException;
import java.util.List;

import static cz.sep.tadeas.ui.VaadinUI.currentUser;

public class TeacherMainUI extends VerticalLayout implements View {

    public static final String NAME = "TeacherMainUI";

    private List<TaskDeliveryWindow> tasksWindows;
    private Grid<TaskDeliveryWindow> taskDeliveryWindowGrid;

    private Grid<Task> taskGrid;
    private List<Task> tasks;

    private Grid<TaskDelivery> taskDeliveryGrid;
    private List<TaskDelivery> tasksDelivery;

    public TeacherMainUI() {
        displayCurrentUserInfo();
        displayTasksDeliveryWindowsPanel();
        displayAllTasks();
        displayTaskDeliveries();
        displayAcceptanceForDeliveries();
    }

    private void displayCurrentUserInfo(){
        Label teacherName = new Label("User name: " + currentUser.getUserName());
        Label teacherEmail = new Label("Email: " + currentUser.getEmail());
        Label teacherRole = new Label("Role: " + currentUser.getRole().getName());

        addComponent(teacherName);
        addComponent(teacherEmail);
        addComponent(teacherRole);
    }

    private void displayTasksDeliveryWindowsPanel(){
        Panel panel = new Panel("Tasks delivery windows");
        addComponent(panel);

        VerticalLayout mainLayout = new VerticalLayout();
        mainLayout.setSizeFull();
        mainLayout.setResponsive(true);

        panel.setContent(mainLayout);
        setComponentAlignment(panel, Alignment.MIDDLE_CENTER);

        mainLayout.addComponent(createGridOfTaskDeliveryWindows());
    }

    private void displayAllTasks(){
        Panel panel = new Panel("All tasks");
        addComponent(panel);

        VerticalLayout mainLayout = new VerticalLayout();
        mainLayout.setSizeFull();
        mainLayout.setResponsive(true);

        panel.setContent(mainLayout);
        setComponentAlignment(panel, Alignment.MIDDLE_CENTER);

        mainLayout.addComponent(createGridOfTasks());
    }

    private void displayTaskDeliveries(){
        Panel panel = new Panel("All tasks deliveries");
        addComponent(panel);

        VerticalLayout mainLayout = new VerticalLayout();
        mainLayout.setSizeFull();
        mainLayout.setResponsive(true);

        panel.setContent(mainLayout);
        setComponentAlignment(panel, Alignment.MIDDLE_CENTER);

        mainLayout.addComponent(createGridOfTaskDeliveries());
    }

    private void displayAcceptanceForDeliveries(){
        for (TaskDelivery taskDelivery : tasksDelivery){
            Panel acceptancePanel = new Panel("Acceptance for id: " + taskDelivery.getId());
            addComponent(acceptancePanel);
            VerticalLayout mainLayout = new VerticalLayout();
            mainLayout.setSizeFull();
            mainLayout.setResponsive(true);

            acceptancePanel.setContent(mainLayout);
            setComponentAlignment(acceptancePanel, Alignment.MIDDLE_CENTER);

            TextArea acceptanceLabel = new TextArea("Info about delivery:");
            acceptanceLabel.setReadOnly(true);
            acceptanceLabel.setValue(taskDelivery.toString());
            acceptanceLabel.setSizeFull();
            mainLayout.addComponent(acceptanceLabel);

            Button acceptButton = new Button("Accept");
            acceptButton.addClickListener(new Button.ClickListener() {
                @Override
                public void buttonClick(Button.ClickEvent clickEvent) {
                    Notification.show("Solution accepted", Notification.Type.HUMANIZED_MESSAGE);
                    RestClient restClient = new RestClient();
                    restClient.put("/delivery/" + ((int) taskDelivery.getId()), "{\"accept\": true}");
                }
            });
            mainLayout.addComponent(acceptButton);
        }
    }

    private void loadAllTaskDeliveries() {
        RestClient restClient = new RestClient();
        String response = restClient.get("/delivery");

        ObjectMapper mapper = new ObjectMapper();

        try {
            tasksDelivery = mapper.readValue(response,
                    new TypeReference<List<TaskDelivery>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Component createGridOfTaskDeliveries() {
        taskDeliveryGrid = new Grid<>(TaskDelivery.class);
        loadAllTaskDeliveries();
        taskDeliveryGrid.setItems(tasksDelivery);
        taskDeliveryGrid.setResponsive(true);
        taskDeliveryGrid.setSizeFull();
        return taskDeliveryGrid;
    }

    private void loadAllTasks() {
        RestClient restClient = new RestClient();
        String response = restClient.get("/task");

        ObjectMapper mapper = new ObjectMapper();

        try {
            tasks = mapper.readValue(response, new TypeReference<List<Task>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Component createGridOfTasks() {
        taskGrid = new Grid<>(Task.class);
        loadAllTasks();
        taskGrid.setItems(tasks);
        taskGrid.setResponsive(true);
        taskGrid.setSizeFull();
        return taskGrid;
    }

    private void loadAllTaskDeliveryWindows() {
        RestClient restClient = new RestClient();
        String response = restClient.get("/window");

        ObjectMapper mapper = new ObjectMapper();

        try {
            tasksWindows = mapper.readValue(response,
                    new TypeReference<List<TaskDeliveryWindow>>() {
                    });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Component createGridOfTaskDeliveryWindows() {
        taskDeliveryWindowGrid = new Grid<>(TaskDeliveryWindow.class);
        loadAllTaskDeliveryWindows();
        taskDeliveryWindowGrid.setItems(tasksWindows);
        taskDeliveryWindowGrid.setResponsive(true);
        taskDeliveryWindowGrid.setSizeFull();
        return taskDeliveryWindowGrid;
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {
        Button logout = new Button("Logout");
        logout.addClickListener(new Button.ClickListener() {
            private static final long serialVersionUID = 1L;

            @Override
            public void buttonClick(Button.ClickEvent event) {
//                getUI().getNavigator().removeView(TeacherMainUI.NAME);
                VaadinSession.getCurrent().setAttribute("user", null);
                Page.getCurrent().setUriFragment("!" + LoginPage.NAME);
            }
        });

        addComponent(logout);
    }
}
