package cz.sep.tadeas;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.sep.tadeas.primitives.Task;

import java.util.List;

import static cz.sep.tadeas.ui.VaadinUI.currentUser;

public class Authentication {

    private String username;
    private String password;

    public Authentication() {
        setUsername("myuser");
        setPassword("mypass");
    }

    private void setUsername(String username) {
        this.username = username;
    }

    private String getUsername() {
        return this.username;
    }

    private void setPassword(String password) {
        this.password = password;
    }

    private String getPassword() {
        return this.password;
    }

    Boolean authenticate(String username, String password) {
        RestClient restClient = new RestClient();
        String response = null;
        try {
            response = restClient.get("/user/login?username=" + username + "&password=" + password);

            ObjectMapper mapper = new ObjectMapper();
            currentUser = mapper.readValue(response, CurrentUser.class);
            currentUser.setUserName(username);
//            currentUser.setPassword(password);
        } catch (Exception e){
            System.err.println("Invalid credentials");
            return false;
        }

        System.out.println("authenticate; " + response);

        return (response != null);
    }

}
