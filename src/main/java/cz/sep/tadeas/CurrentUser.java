package cz.sep.tadeas;

import cz.sep.tadeas.primitives.User;
import cz.sep.tadeas.primitives.UserGroup;
import cz.sep.tadeas.primitives.UserRole;

import java.util.ArrayList;

public class CurrentUser extends User {

    private float id;
    private String userName;
    private String firstName, lastName;
    private String email;
    private String password;
    private UserRole role;
    private ArrayList<UserGroup> groups;
    private String sessionId;

    public CurrentUser() {
    }

    public CurrentUser(float id, String email, UserRole role) {
        this.id = id;
        this.email = email;
        this.role = role;
    }

    public boolean isStudent() {
        return role.isStudent();
    }

    public boolean isTeacher() {
        return role.isTeacher();
    }

    public float getId() {
        return id;
    }

    public void setId(float id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public ArrayList<UserGroup> getGroups() {
        return groups;
    }

    public void setGroups(ArrayList<UserGroup> groups) {
        this.groups = groups;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    @Override
    public String toString() {
        return "userName: " + userName +
                "\nemail: " + email +
                "\nrole: " + role.getName();
    }
}
